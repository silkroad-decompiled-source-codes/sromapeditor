﻿// Decompiled with Syinea's decompiler
// Type: SroMapEditor.Terrain
// Assembly: SroMapEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 02A4606F-D062-4F97-A318-DADC65FCD6AD
// Assembly location: C:\Users\Syinea\Downloads\Mapeditor\Mapeditor\SroMapEditor.exe

using OpenTK.Graphics.OpenGL;
using SroMapEditor.DDSReader;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SroMapEditor
{
  internal class Terrain
  {
    private string pathPrefix = "Map\\tile2d\\";
    private float[][][] Heights = new float[36][][];
    private int[][][] Tex = new int[36][][];
    private List<int> FileTexIDs = new List<int>();
    private int[] TexIDs;
    private string[] TexPaths;

    public Terrain(string path)
    {
      this.getTexPaths();
      BinaryReader binaryReader = new BinaryReader((Stream) File.Open(path, FileMode.Open));
      binaryReader.BaseStream.Position = 12L;
      for (int index1 = 0; index1 < 36; ++index1)
      {
        this.Heights[index1] = new float[17][];
        this.Tex[index1] = new int[17][];
        binaryReader.BaseStream.Position += 6L;
        for (int index2 = 0; index2 < 17; ++index2)
        {
          this.Heights[index1][index2] = new float[17];
          this.Tex[index1][index2] = new int[17];
        }
        for (int index2 = 0; index2 < 17; ++index2)
        {
          for (int index3 = 0; index3 < 17; ++index3)
          {
            this.Heights[index1][index3][index2] = binaryReader.ReadSingle();
            this.Tex[index1][index3][index2] = (int) binaryReader.ReadByte();
            binaryReader.BaseStream.Position += 2L;
          }
        }
        binaryReader.BaseStream.Position += 546L;
      }
      binaryReader.Close();
      binaryReader.Dispose();
      this.getTex();
    }

    private void getTex()
    {
      List<int> intList1 = new List<int>();
      List<int> intList2 = new List<int>();
      for (int index1 = 0; index1 < 36; ++index1)
      {
        for (int index2 = 0; index2 < 17; ++index2)
        {
          for (int index3 = 0; index3 < 17; ++index3)
          {
            if (!intList1.Contains(this.Tex[index1][index2][index3]))
            {
              intList1.Add(this.Tex[index1][index2][index3]);
              int texture = GL.GenTexture();
              GL.BindTexture(TextureTarget.Texture2D, texture);
              string path = this.pathPrefix + this.TexPaths[this.Tex[index1][index2][index3]];
              byte[] numArray = new byte[0];
              System.Drawing.Bitmap bitmapImage = new DDSImage(File.Exists(path) ? ((IEnumerable<byte>) File.ReadAllBytes(path)).Skip<byte>(20).ToArray<byte>() : File.ReadAllBytes(path.Replace(".ddj", ".dds"))).BitmapImage;
              BitmapData bitmapdata = bitmapImage.LockBits(new Rectangle(0, 0, bitmapImage.Width, bitmapImage.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
              GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitmapdata.Width, bitmapdata.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bitmapdata.Scan0);
              bitmapImage.UnlockBits(bitmapdata);
              GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, 9729);
              this.FileTexIDs.Add(this.Tex[index1][index2][index3]);
              intList2.Add(texture);
            }
          }
        }
      }
      this.TexIDs = intList2.ToArray();
      intList2.Clear();
    }

    public void Draw()
    {
      GL.Color3(byte.MaxValue, byte.MaxValue, byte.MaxValue);
      for (int index1 = 0; index1 < 36; ++index1)
      {
        for (int index2 = 0; index2 < 16; ++index2)
        {
          for (int index3 = 0; index3 < 16; ++index3)
          {
            GL.Enable(EnableCap.Texture2D);
            GL.BindTexture(TextureTarget.Texture2D, this.TexIDs[this.FileTexIDs.IndexOf(this.Tex[index1][index2][index3])]);
            GL.Begin(BeginMode.TriangleStrip);
            GL.TexCoord2((float) index2 / 2f, (float) index3 / 2f);
            GL.Vertex3((double) (index2 * 20 + index1 % 6 * 320), (double) (index3 * 20) + Math.Floor((double) index1 / 6.0) * 320.0, (double) this.Heights[index1][index2][index3]);
            GL.TexCoord2((float) (index2 + 1) / 2f, (float) index3 / 2f);
            GL.Vertex3((double) ((index2 + 1) * 20 + index1 % 6 * 320), (double) (index3 * 20) + Math.Floor((double) index1 / 6.0) * 320.0, (double) this.Heights[index1][index2 + 1][index3]);
            GL.TexCoord2((float) index2 / 2f, (float) (index3 + 1) / 2f);
            GL.Vertex3((double) (index2 * 20 + index1 % 6 * 320), (double) ((index3 + 1) * 20) + Math.Floor((double) index1 / 6.0) * 320.0, (double) this.Heights[index1][index2][index3 + 1]);
            GL.TexCoord2((float) (index2 + 1) / 2f, (float) (index3 + 1) / 2f);
            GL.Vertex3((double) ((index2 + 1) * 20 + index1 % 6 * 320), (double) ((index3 + 1) * 20) + Math.Floor((double) index1 / 6.0) * 320.0, (double) this.Heights[index1][index2 + 1][index3 + 1]);
            GL.End();
            GL.Disable(EnableCap.Texture2D);
          }
        }
      }
    }

    private void getTexPaths()
    {
      if (!File.Exists("Map\\tile2d.ifo"))
      {
        int num = (int) MessageBox.Show("Could not find tile2d.ifo. Terminating application.");
        Environment.Exit(1);
      }
      string[] strArray = File.ReadAllLines("Map\\tile2d.ifo");
      this.TexPaths = new string[strArray.Length - 2];
      for (int index = 0; index < this.TexPaths.Length; ++index)
      {
        string str = strArray[index + 2].Split(' ')[3];
        this.TexPaths[index] = str.Substring(1, str.Length - 2);
      }
    }
  }
}
