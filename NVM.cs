﻿// Decompiled with Syinea's decompiler
// Type: SroMapEditor.NVM
// Assembly: SroMapEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 02A4606F-D062-4F97-A318-DADC65FCD6AD
// Assembly location: C:\Users\Syinea\Downloads\Mapeditor\Mapeditor\SroMapEditor.exe

using System;
using System.Collections.Generic;
using System.IO;

namespace SroMapEditor
{
  internal class NVM
  {
    public int entryNum;
    public string filepath;
    private NVMEntity[] entities;
    private byte[] otherData;

    public NVM(string path)
    {
      this.filepath = path;
      BinaryReader binaryReader = new BinaryReader((Stream) File.Open(path, FileMode.Open));
      binaryReader.BaseStream.Position = 12L;
      this.entryNum = (int) binaryReader.ReadInt16();
      this.entities = new NVMEntity[this.entryNum];
      for (int index = 0; index < this.entryNum; ++index)
      {
        NVMEntity nvmEntity = new NVMEntity();
        nvmEntity.id = binaryReader.ReadInt32();
        nvmEntity.X = binaryReader.ReadSingle();
        nvmEntity.Y = binaryReader.ReadSingle();
        nvmEntity.Z = binaryReader.ReadSingle();
        nvmEntity.UK1 = binaryReader.ReadInt16();
        nvmEntity.UK2 = binaryReader.ReadSingle();
        nvmEntity.UK3 = binaryReader.ReadInt16();
        nvmEntity.UK4 = binaryReader.ReadInt16();
        nvmEntity.UK5 = binaryReader.ReadInt16();
        nvmEntity.Grid = binaryReader.ReadInt16();
        int num = (int) binaryReader.ReadInt16();
        nvmEntity.xtra = binaryReader.ReadBytes(num * 6);
        this.entities[index] = nvmEntity;
      }
      this.otherData = binaryReader.ReadBytes((int) binaryReader.BaseStream.Length - (int) binaryReader.BaseStream.Position);
      binaryReader.Close();
    }

    public void setEntities(List<MapObject> objects, string pathPref, string[] bsrPaths)
    {
      List<NVMEntity> nvmEntityList = new List<NVMEntity>();
      for (int index = 0; index < objects.Count; ++index)
      {
        if (bsrPaths[objects[index].nameI].Contains(".bsr"))
        {
          BinaryReader binaryReader = new BinaryReader((Stream) File.Open(pathPref + bsrPaths[objects[index].nameI], FileMode.Open));
          binaryReader.BaseStream.Position = 68L;
          int num1 = binaryReader.ReadInt32();
          binaryReader.BaseStream.Position += (long) (48 + num1);
          int num2 = binaryReader.ReadInt32();
          binaryReader.Close();
          if (num2 != 0)
            nvmEntityList.Add(new NVMEntity()
            {
              id = objects[index].nameI,
              X = objects[index].X,
              Y = objects[index].Y,
              Z = objects[index].Z,
              UK1 = !(objects[index].Unknown1 == "FFFF") ? (short) 0 : (short) -1,
              UK2 = objects[index].Theta,
              UK3 = (short) objects[index].ID,
              UK4 = (short) 0,
              UK5 = !(objects[index].Unknown2 != "0100") ? (short) 0 : (short) 1
            });
        }
      }
      this.entities = nvmEntityList.ToArray();
    }

    public void saveNVM(string path, int OX, int OY)
    {
      BinaryWriter binaryWriter = new BinaryWriter((Stream) File.Open(path, FileMode.Create));
      binaryWriter.Write("JMXVNVM 1000".ToCharArray());
      binaryWriter.Write((short) this.entities.Length);
      for (int index = 0; index < this.entities.Length; ++index)
      {
        binaryWriter.Write(this.entities[index].id);
        float x = this.entities[index].X;
        if ((double) this.entities[index].X > 1920.0)
          x -= 1920f;
        else if ((double) this.entities[index].X < 0.0)
          x += 1920f;
        binaryWriter.Write(x);
        binaryWriter.Write(this.entities[index].Y);
        float z = this.entities[index].Z;
        if ((double) this.entities[index].Z > 1920.0)
          z -= 1920f;
        else if ((double) this.entities[index].Z < 0.0)
          z += 1920f;
        binaryWriter.Write(z);
        binaryWriter.Write(this.entities[index].UK1);
        binaryWriter.Write(this.entities[index].UK2);
        binaryWriter.Write(this.entities[index].UK3);
        binaryWriter.Write(this.entities[index].UK4);
        binaryWriter.Write(this.entities[index].UK5);
        binaryWriter.Write((byte) ((double) OX + Math.Floor((double) this.entities[index].X / 1920.0)));
        binaryWriter.Write((byte) ((double) OY + Math.Floor((double) this.entities[index].Z / 1920.0)));
        binaryWriter.Write((short) 0);
      }
      binaryWriter.Write(this.otherData);
      binaryWriter.Close();
    }
  }
}
