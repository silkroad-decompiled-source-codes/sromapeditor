﻿// Decompiled with Syinea's decompiler
// Type: SroMapEditor.NVMEntity
// Assembly: SroMapEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 02A4606F-D062-4F97-A318-DADC65FCD6AD
// Assembly location: C:\Users\Syinea\Downloads\Mapeditor\Mapeditor\SroMapEditor.exe

namespace SroMapEditor
{
  internal struct NVMEntity
  {
    public int id;
    public float X;
    public float Y;
    public float Z;
    public short UK1;
    public float UK2;
    public short UK3;
    public short UK4;
    public short UK5;
    public short Grid;
    public byte[] xtra;
  }
}
