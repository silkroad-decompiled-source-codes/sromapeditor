﻿// Decompiled with Syinea's decompiler
// Type: SroMapEditor.Properties.Resources
// Assembly: SroMapEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 02A4606F-D062-4F97-A318-DADC65FCD6AD
// Assembly location: C:\Users\Syinea\Downloads\Mapeditor\Mapeditor\SroMapEditor.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SroMapEditor.Properties
{
  [CompilerGenerated]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (SroMapEditor.Properties.Resources.resourceMan == null)
          SroMapEditor.Properties.Resources.resourceMan = new ResourceManager("SroMapEditor.Properties.Resources", typeof (SroMapEditor.Properties.Resources).Assembly);
        return SroMapEditor.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return SroMapEditor.Properties.Resources.resourceCulture;
      }
      set
      {
        SroMapEditor.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
