﻿// Decompiled with Syinea's decompiler
// Type: SroMapEditor.MapObject
// Assembly: SroMapEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 02A4606F-D062-4F97-A318-DADC65FCD6AD
// Assembly location: C:\Users\Syinea\Downloads\Mapeditor\Mapeditor\SroMapEditor.exe

using OpenTK.Graphics.OpenGL;
using SroMapEditor.DDSReader;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace SroMapEditor
{
  internal class MapObject
  {
    public int nameI;
    public float X;
    public float Y;
    public float Z;
    public float Theta;
    public int ID;
    public List<int> groups;
    public string Unknown1;
    public string Unknown2;
    public bool DistFade;
    public string materialPath;
    private string pathPrefix;
    private float[][,] Verts;
    private float[][,] UV;
    private int[][,] Faces;
    private string[] Tex;
    private int[] TexIDs;
    private int readingMesh;
    public float[] boundingBoxp1;
    public float[] boundingBoxp2;
    public float[] rotBoundingBoxp1;
    public float[] rotBoundingBoxp2;

    public MapObject()
    {
      this.groups = new List<int>();
    }

    public void LoadFiles(string pathPref, string path)
    {
      this.readingMesh = 0;
      this.pathPrefix = pathPref;
      BinaryReader binaryReader = new BinaryReader((Stream) File.Open(pathPref + path, FileMode.Open));
      binaryReader.BaseStream.Position = 12L;
      int num1 = binaryReader.ReadInt32();
      int num2 = binaryReader.ReadInt32();
      int num3 = binaryReader.ReadInt32();
      binaryReader.BaseStream.Position = (long) (num1 + 8);
      int count1 = binaryReader.ReadInt32();
      string str1 = new string(binaryReader.ReadChars(count1));
      binaryReader.BaseStream.Position = (long) num2;
      int length1 = binaryReader.ReadInt32();
      string[] strArray = new string[length1];
      for (int index = 0; index < length1; ++index)
      {
        int count2 = binaryReader.ReadInt32();
        if (count2 < 20)
          count2 = binaryReader.ReadInt32();
        strArray[index] = new string(binaryReader.ReadChars(count2));
      }
      binaryReader.BaseStream.Position = (long) num3;
      this.DistFade = binaryReader.ReadInt32() != 0;
      binaryReader.Close();
      this.materialPath = pathPref + str1;
      binaryReader.Close();
      int length2 = strArray.Length;
      this.Verts = new float[length2][,];
      this.UV = new float[length2][,];
      this.Faces = new int[length2][,];
      this.Tex = new string[length2];
      foreach (string str2 in strArray)
        this.readModel(pathPref + str2);
      float num4 = this.Verts[0][0, 0];
      float num5 = this.Verts[0][0, 1];
      float num6 = this.Verts[0][0, 2];
      float num7 = this.Verts[0][0, 0];
      float num8 = this.Verts[0][0, 1];
      float num9 = this.Verts[0][0, 2];
      for (int index1 = 0; index1 < this.Verts.Length; ++index1)
      {
        for (int index2 = 0; index2 < this.Verts[index1].Length / 3; ++index2)
        {
          if ((double) this.Verts[index1][index2, 0] < (double) num4)
            num4 = this.Verts[index1][index2, 0];
          if ((double) this.Verts[index1][index2, 1] < (double) num5)
            num5 = this.Verts[index1][index2, 1];
          if ((double) this.Verts[index1][index2, 2] < (double) num6)
            num6 = this.Verts[index1][index2, 2];
          if ((double) this.Verts[index1][index2, 0] > (double) num7)
            num7 = this.Verts[index1][index2, 0];
          if ((double) this.Verts[index1][index2, 1] > (double) num8)
            num8 = this.Verts[index1][index2, 1];
          if ((double) this.Verts[index1][index2, 2] > (double) num9)
            num9 = this.Verts[index1][index2, 2];
        }
      }
      this.boundingBoxp1 = new float[3]{ num4, num5, num6 };
      this.boundingBoxp2 = new float[3]{ num7, num8, num9 };
      this.rotBoundingBoxp1 = new float[3]
      {
        num4,
        num5,
        num6
      };
      this.rotBoundingBoxp2 = new float[3]
      {
        num7,
        num8,
        num9
      };
      this.setRotation(this.Theta);
    }

    public void Draw()
    {
      if (this.Faces == null)
        return;
      for (int index1 = 0; index1 < this.Faces.Length; ++index1)
      {
        GL.Enable(EnableCap.Texture2D);
        if (this.TexIDs != null)
          GL.BindTexture(TextureTarget.Texture2D, this.TexIDs[index1]);
        GL.Begin(BeginMode.Triangles);
        if (this.Faces[index1] != null)
        {
          for (int index2 = 0; index2 < this.Faces[index1].Length / 3; ++index2)
          {
            GL.TexCoord2(this.UV[index1][this.Faces[index1][index2, 0], 0], this.UV[index1][this.Faces[index1][index2, 0], 1]);
            GL.Vertex3(this.Verts[index1][this.Faces[index1][index2, 0], 0], this.Verts[index1][this.Faces[index1][index2, 0], 2], this.Verts[index1][this.Faces[index1][index2, 0], 1]);
            GL.TexCoord2(this.UV[index1][this.Faces[index1][index2, 1], 0], this.UV[index1][this.Faces[index1][index2, 1], 1]);
            GL.Vertex3(this.Verts[index1][this.Faces[index1][index2, 1], 0], this.Verts[index1][this.Faces[index1][index2, 1], 2], this.Verts[index1][this.Faces[index1][index2, 1], 1]);
            GL.TexCoord2(this.UV[index1][this.Faces[index1][index2, 2], 0], this.UV[index1][this.Faces[index1][index2, 2], 1]);
            GL.Vertex3(this.Verts[index1][this.Faces[index1][index2, 2], 0], this.Verts[index1][this.Faces[index1][index2, 2], 2], this.Verts[index1][this.Faces[index1][index2, 2], 1]);
          }
        }
        GL.End();
        GL.Disable(EnableCap.Texture2D);
      }
    }

    public void drawBoundingBox()
    {
      if (this.boundingBoxp1 == null)
        return;
      GL.Color3(byte.MaxValue, (byte) 0, (byte) 0);
      GL.Begin(BeginMode.LineStrip);
      GL.Vertex3(this.boundingBoxp1[0], this.boundingBoxp1[2], this.boundingBoxp1[1]);
      GL.Vertex3(this.boundingBoxp2[0], this.boundingBoxp1[2], this.boundingBoxp1[1]);
      GL.Vertex3(this.boundingBoxp2[0], this.boundingBoxp1[2], this.boundingBoxp2[1]);
      GL.Vertex3(this.boundingBoxp1[0], this.boundingBoxp1[2], this.boundingBoxp2[1]);
      GL.Vertex3(this.boundingBoxp1[0], this.boundingBoxp2[2], this.boundingBoxp2[1]);
      GL.Vertex3(this.boundingBoxp2[0], this.boundingBoxp2[2], this.boundingBoxp2[1]);
      GL.Vertex3(this.boundingBoxp2[0], this.boundingBoxp2[2], this.boundingBoxp1[1]);
      GL.Vertex3(this.boundingBoxp1[0], this.boundingBoxp2[2], this.boundingBoxp1[1]);
      GL.Vertex3(this.boundingBoxp1[0], this.boundingBoxp1[2], this.boundingBoxp1[1]);
      GL.End();
      GL.Begin(BeginMode.Lines);
      GL.Vertex3(this.boundingBoxp1[0], this.boundingBoxp2[2], this.boundingBoxp1[1]);
      GL.Vertex3(this.boundingBoxp1[0], this.boundingBoxp2[2], this.boundingBoxp2[1]);
      GL.Vertex3(this.boundingBoxp1[0], this.boundingBoxp1[2], this.boundingBoxp1[1]);
      GL.Vertex3(this.boundingBoxp1[0], this.boundingBoxp1[2], this.boundingBoxp2[1]);
      GL.Vertex3(this.boundingBoxp2[0], this.boundingBoxp1[2], this.boundingBoxp1[1]);
      GL.Vertex3(this.boundingBoxp2[0], this.boundingBoxp2[2], this.boundingBoxp1[1]);
      GL.Vertex3(this.boundingBoxp2[0], this.boundingBoxp1[2], this.boundingBoxp2[1]);
      GL.Vertex3(this.boundingBoxp2[0], this.boundingBoxp2[2], this.boundingBoxp2[1]);
      GL.End();
      GL.Color3(byte.MaxValue, byte.MaxValue, byte.MaxValue);
    }

    public void drawGroups()
    {
      GL.Color4(byte.MaxValue, byte.MaxValue, (byte) 0, (byte) 100);
      for (int index = 0; index < this.groups.Count; ++index)
      {
        GL.Begin(BeginMode.TriangleStrip);
        GL.Vertex3((double) (this.groups[index] % 24 * 80), Math.Floor((double) this.groups[index] / 24.0) * 320.0, 500.0);
        GL.Vertex3((double) ((this.groups[index] % 24 + 1) * 80), Math.Floor((double) this.groups[index] / 24.0) * 320.0, 500.0);
        GL.Vertex3((double) (this.groups[index] % 24 * 80), (Math.Floor((double) this.groups[index] / 24.0) + 1.0) * 320.0, 500.0);
        GL.Vertex3((double) ((this.groups[index] % 24 + 1) * 80), (Math.Floor((double) this.groups[index] / 24.0) + 1.0) * 320.0, 500.0);
        GL.End();
      }
      GL.Color4(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
    }

    public void setRotation(float rot)
    {
      this.Theta = rot;
      this.rotBoundingBoxp2[0] = (float) ((double) this.boundingBoxp1[0] * Math.Cos((double) this.Theta) - (double) this.boundingBoxp1[2] * Math.Sin((double) this.Theta));
      this.rotBoundingBoxp2[2] = (float) ((double) this.boundingBoxp1[0] * Math.Sin((double) this.Theta) + (double) this.boundingBoxp1[2] * Math.Cos((double) this.Theta));
      this.rotBoundingBoxp1[0] = (float) ((double) this.boundingBoxp2[0] * Math.Cos((double) this.Theta) - (double) this.boundingBoxp2[2] * Math.Sin((double) this.Theta));
      this.rotBoundingBoxp1[2] = (float) ((double) this.boundingBoxp2[0] * Math.Sin((double) this.Theta) + (double) this.boundingBoxp2[2] * Math.Cos((double) this.Theta));
      if ((double) this.rotBoundingBoxp1[2] > (double) this.rotBoundingBoxp2[2])
      {
        float num = this.rotBoundingBoxp1[2];
        this.rotBoundingBoxp1[2] = this.rotBoundingBoxp2[2];
        this.rotBoundingBoxp2[2] = num;
      }
      if ((double) this.rotBoundingBoxp1[0] <= (double) this.rotBoundingBoxp2[0])
        return;
      float num1 = this.rotBoundingBoxp1[0];
      this.rotBoundingBoxp1[0] = this.rotBoundingBoxp2[0];
      this.rotBoundingBoxp2[0] = num1;
    }

    public void calcGroups()
    {
      if (this.boundingBoxp1 != null)
      {
        int num1 = (int) Math.Floor(((double) this.rotBoundingBoxp1[0] + (double) this.X) / 80.0);
        int num2 = (int) Math.Floor(((double) this.rotBoundingBoxp1[2] + (double) this.Z) / 320.0);
        int num3 = (int) Math.Floor(((double) this.rotBoundingBoxp2[0] + (double) this.X) / 80.0);
        int num4 = (int) Math.Floor(((double) this.rotBoundingBoxp2[2] + (double) this.Z) / 320.0);
        List<int> intList1 = new List<int>();
        if (num1 < 2)
          num1 = 2;
        if (num3 < 2)
          num3 = 2;
        if (num1 > 23)
          num1 = 23;
        if (num3 > 23)
          num3 = 23;
        if (num1 != num3)
        {
          if ((num1 - 2) % 4 != 0)
          {
            while ((num1 - 2) % 4 != 0)
            {
              --num1;
              if (num1 < 0)
              {
                num1 = 2;
                break;
              }
            }
          }
          if ((num3 - 2) % 4 != 0)
          {
            while ((num3 - 2) % 4 != 0)
            {
              ++num3;
              if (num3 > 23)
              {
                num3 = 23;
                break;
              }
            }
          }
          for (int index = 0; index < (num3 - num1) / 4 + 1; ++index)
            intList1.Add(num1 + 4 * index);
        }
        else
        {
          if ((num1 - 2) % 4 != 0)
          {
            while ((num1 - 2) % 4 != 0)
            {
              ++num1;
              if (num1 < 23)
              {
                num1 = 23;
                break;
              }
            }
          }
          intList1.Add(num1);
        }
        List<int> intList2 = new List<int>();
        if (num2 < 0)
          num2 = 0;
        if (num4 < 0)
          num4 = 0;
        if (num2 > 5)
          num2 = 5;
        if (num4 > 5)
          num4 = 5;
        if (num2 != num4)
        {
          for (int index = 0; index < num4 - num2 + 1; ++index)
            intList2.Add(num2 + index);
        }
        else
          intList2.Add(num2);
        for (int index1 = 0; index1 < intList2.Count; ++index1)
        {
          for (int index2 = 0; index2 < intList1.Count; ++index2)
            this.groups.Add(intList2[index1] * 24 + intList1[index2]);
        }
      }
      else
      {
        int num1 = (int) Math.Floor((double) this.X / 80.0);
        while ((num1 - 2) % 4 != 0)
        {
          ++num1;
          if (num1 < 23)
          {
            num1 = 23;
            break;
          }
        }
        int num2 = (int) Math.Floor((double) this.Z / 320.0);
        this.groups.Add(num1 + 24 * num2);
      }
    }

    public MeshTexture[] readMaterial()
    {
      BinaryReader binaryReader = new BinaryReader((Stream) File.Open(this.materialPath, FileMode.Open));
      binaryReader.BaseStream.Position = 12L;
      int length = binaryReader.ReadInt32();
      MeshTexture[] meshTextureArray = new MeshTexture[length];
      for (int index = 0; index < length; ++index)
      {
        int count1 = binaryReader.ReadInt32();
        string tName = new string(binaryReader.ReadChars(count1));
        binaryReader.BaseStream.Position += 72L;
        int count2 = binaryReader.ReadInt32();
        string source = new string(binaryReader.ReadChars(count2));
        binaryReader.BaseStream.Position += 7L;
        if (source != "")
        {
          int num = GL.GenTexture();
          GL.BindTexture(TextureTarget.Texture2D, num);
          string path = !source.Contains<char>('\\') ? this.materialPath.Substring(0, this.materialPath.LastIndexOf('\\') + 1) + source : this.pathPrefix + source;
          byte[] numArray = new byte[0];
          System.Drawing.Bitmap bitmapImage = new DDSImage(File.Exists(path) ? ((IEnumerable<byte>) File.ReadAllBytes(path)).Skip<byte>(20).ToArray<byte>() : File.ReadAllBytes(path.Replace(".ddj", ".dds"))).BitmapImage;
          BitmapData bitmapdata = bitmapImage.LockBits(new Rectangle(0, 0, bitmapImage.Width, bitmapImage.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
          GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitmapdata.Width, bitmapdata.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bitmapdata.Scan0);
          bitmapImage.UnlockBits(bitmapdata);
          GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, 9729);
          GL.TexEnv(TextureEnvTarget.TextureEnv, TextureEnvParameter.TextureEnvMode, 8448);
          meshTextureArray[index] = new MeshTexture(tName, num);
        }
      }
      binaryReader.Close();
      binaryReader.Dispose();
      return meshTextureArray;
    }

    private void readModel(string path)
    {
      BinaryReader binaryReader = new BinaryReader((Stream) File.Open(path, FileMode.Open));
      binaryReader.BaseStream.Position = 12L;
      int num1 = binaryReader.ReadInt32();
      binaryReader.BaseStream.Position += 4L;
      int num2 = binaryReader.ReadInt32();
      binaryReader.BaseStream.Position = 72L;
      int num3 = binaryReader.ReadInt32();
      binaryReader.BaseStream.Position += (long) num3;
      int count = binaryReader.ReadInt32();
      this.Tex[this.readingMesh] = new string(binaryReader.ReadChars(count));
      binaryReader.BaseStream.Position = (long) num1;
      int length1 = binaryReader.ReadInt32();
      this.Verts[this.readingMesh] = new float[length1, 3];
      this.UV[this.readingMesh] = new float[length1, 2];
      for (int index = 0; index < length1; ++index)
      {
        this.Verts[this.readingMesh][index, 0] = binaryReader.ReadSingle();
        this.Verts[this.readingMesh][index, 1] = binaryReader.ReadSingle();
        this.Verts[this.readingMesh][index, 2] = binaryReader.ReadSingle();
        binaryReader.BaseStream.Position += 12L;
        this.UV[this.readingMesh][index, 0] = binaryReader.ReadSingle();
        this.UV[this.readingMesh][index, 1] = binaryReader.ReadSingle();
        binaryReader.BaseStream.Position += 16L;
        int num4 = Math.Abs(binaryReader.ReadInt32());
        if (num4 < 0 || num4 > 10)
          binaryReader.BaseStream.Position -= 8L;
      }
      binaryReader.BaseStream.Position = (long) num2;
      int length2 = binaryReader.ReadInt32();
      this.Faces[this.readingMesh] = new int[length2, 3];
      for (int index1 = 0; index1 < length2; ++index1)
      {
        for (int index2 = 0; index2 < 3; ++index2)
          this.Faces[this.readingMesh][index1, index2] = (int) binaryReader.ReadInt16();
      }
      binaryReader.Close();
      ++this.readingMesh;
    }

    public void FindTex(List<string> texNames, List<int> texIDs)
    {
      if (this.Tex == null)
        return;
      this.TexIDs = new int[this.Tex.Length];
      for (int index = 0; index < this.Tex.Length; ++index)
      {
        if (texNames.IndexOf(this.Tex[index]) != -1)
          this.TexIDs[index] = texIDs[texNames.IndexOf(this.Tex[index])];
      }
    }
  }
}
