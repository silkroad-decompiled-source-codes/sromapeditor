﻿// Decompiled with Syinea's decompiler
// Type: SroMapEditor.Form1
// Assembly: SroMapEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 02A4606F-D062-4F97-A318-DADC65FCD6AD
// Assembly location: C:\Users\Syinea\Downloads\Mapeditor\Mapeditor\SroMapEditor.exe

using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SroMapEditor
{
  public class Form1 : Form
  {
    private string pathPref = "Data\\";
    private List<string> texNames = new List<string>();
    private List<int> texIDs = new List<int>();
    private double zoom = 1.0;
    private int selectedObj = -1;
    private IContainer components;
    private GLControl glControl1;
    private Button button1;
    private Button button2;
    private OpenFileDialog openFileDialog1;
    private Label label1;
    private ListBox listBox1;
    private ListBox listBox2;
    private Label label2;
    private ListBox listBox3;
    private Label label3;
    private GroupBox groupBox1;
    private GroupBox groupBox2;
    private NumericUpDown numericUpDown1;
    private TrackBar trackBar1;
    private Label label6;
    private Label label5;
    private Label label4;
    private NumericUpDown numericUpDown3;
    private NumericUpDown numericUpDown2;
    private TextBox textBox1;
    private Label label7;
    private Button button3;
    private Label label10;
    private Label label9;
    private Label label8;
    private TextBox textBox2;
    private CheckBox checkBox3;
    private CheckBox checkBox2;
    private CheckBox checkBox1;
    private GroupBox groupBox3;
    private Button button6;
    private Button button5;
    private Button button4;
    private SaveFileDialog saveFileDialog1;
    private CheckBox checkBox5;
    private CheckBox checkBox4;
    private CheckBox checkBox6;
    private Button button7;
    private Button button8;
    private CheckBox checkBox7;
    private FolderBrowserDialog folderBrowserDialog1;
    private SaveFileDialog saveFileDialog2;
    private bool mapLoaded;
    private ObjectNames ObjNames;
    private List<MapObject> Objects;
    private bool dragging;
    private Point dragStart;
    private int rotHor;
    private int rotVert;
    private Form2 form2;
    private Terrain terrainmap;
    private int OX;
    private int OY;
    private NVM nvmFile;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (Form1));
      this.glControl1 = new GLControl();
      this.button1 = new Button();
      this.button2 = new Button();
      this.openFileDialog1 = new OpenFileDialog();
      this.label1 = new Label();
      this.listBox1 = new ListBox();
      this.listBox2 = new ListBox();
      this.label2 = new Label();
      this.listBox3 = new ListBox();
      this.label3 = new Label();
      this.groupBox1 = new GroupBox();
      this.checkBox7 = new CheckBox();
      this.checkBox5 = new CheckBox();
      this.checkBox4 = new CheckBox();
      this.checkBox3 = new CheckBox();
      this.checkBox2 = new CheckBox();
      this.checkBox1 = new CheckBox();
      this.groupBox2 = new GroupBox();
      this.checkBox6 = new CheckBox();
      this.button3 = new Button();
      this.label10 = new Label();
      this.label9 = new Label();
      this.label8 = new Label();
      this.textBox2 = new TextBox();
      this.textBox1 = new TextBox();
      this.label7 = new Label();
      this.label6 = new Label();
      this.label5 = new Label();
      this.label4 = new Label();
      this.numericUpDown3 = new NumericUpDown();
      this.numericUpDown2 = new NumericUpDown();
      this.numericUpDown1 = new NumericUpDown();
      this.trackBar1 = new TrackBar();
      this.groupBox3 = new GroupBox();
      this.button6 = new Button();
      this.button5 = new Button();
      this.button4 = new Button();
      this.saveFileDialog1 = new SaveFileDialog();
      this.button7 = new Button();
      this.button8 = new Button();
      this.folderBrowserDialog1 = new FolderBrowserDialog();
      this.saveFileDialog2 = new SaveFileDialog();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.numericUpDown3.BeginInit();
      this.numericUpDown2.BeginInit();
      this.numericUpDown1.BeginInit();
      this.trackBar1.BeginInit();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      this.glControl1.BackColor = Color.Black;
      this.glControl1.Location = new Point(0, 0);
      this.glControl1.Name = "glControl1";
      this.glControl1.Size = new Size(745, 630);
      this.glControl1.TabIndex = 0;
      this.glControl1.VSync = false;
      this.glControl1.Load += new EventHandler(this.glControl1_Load);
      this.glControl1.MouseDown += new MouseEventHandler(this.onGLMouseDown);
      this.glControl1.MouseMove += new MouseEventHandler(this.onGLMouseMove);
      this.glControl1.MouseUp += new MouseEventHandler(this.onGLMouseUp);
      this.button1.Location = new Point(754, 3);
      this.button1.Name = "button1";
      this.button1.Size = new Size(148, 22);
      this.button1.TabIndex = 1;
      this.button1.Text = "Open Map";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Enabled = false;
      this.button2.Location = new Point(754, 24);
      this.button2.Name = "button2";
      this.button2.Size = new Size(148, 22);
      this.button2.TabIndex = 2;
      this.button2.Text = "Save Map";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.openFileDialog1.Filter = "Object Maps|*.o2";
      this.label1.AutoSize = true;
      this.label1.Location = new Point(908, 10);
      this.label1.Name = "label1";
      this.label1.Size = new Size(49, 13);
      this.label1.TabIndex = 3;
      this.label1.Text = "Buildings";
      this.listBox1.FormattingEnabled = true;
      this.listBox1.Location = new Point(911, 26);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new Size(145, 225);
      this.listBox1.TabIndex = 4;
      this.listBox1.SelectedIndexChanged += new EventHandler(this.listBox1_SelectedIndexChanged);
      this.listBox2.FormattingEnabled = true;
      this.listBox2.Location = new Point(911, 273);
      this.listBox2.Name = "listBox2";
      this.listBox2.Size = new Size(145, 173);
      this.listBox2.TabIndex = 6;
      this.listBox2.SelectedIndexChanged += new EventHandler(this.listBox2_SelectedIndexChanged);
      this.label2.Location = new Point(908, 257);
      this.label2.Name = "label2";
      this.label2.Size = new Size(49, 13);
      this.label2.TabIndex = 5;
      this.label2.Text = "Nature";
      this.listBox3.FormattingEnabled = true;
      this.listBox3.Location = new Point(911, 467);
      this.listBox3.Name = "listBox3";
      this.listBox3.Size = new Size(145, 147);
      this.listBox3.TabIndex = 8;
      this.listBox3.SelectedIndexChanged += new EventHandler(this.listBox3_SelectedIndexChanged);
      this.label3.Location = new Point(908, 451);
      this.label3.Name = "label3";
      this.label3.Size = new Size(49, 13);
      this.label3.TabIndex = 7;
      this.label3.Text = "Other";
      this.groupBox1.Controls.Add((Control) this.checkBox7);
      this.groupBox1.Controls.Add((Control) this.checkBox5);
      this.groupBox1.Controls.Add((Control) this.checkBox4);
      this.groupBox1.Controls.Add((Control) this.checkBox3);
      this.groupBox1.Controls.Add((Control) this.checkBox2);
      this.groupBox1.Controls.Add((Control) this.checkBox1);
      this.groupBox1.Location = new Point(751, 86);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(154, 154);
      this.groupBox1.TabIndex = 9;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Options";
      this.checkBox7.AutoSize = true;
      this.checkBox7.Location = new Point(12, 131);
      this.checkBox7.Name = "checkBox7";
      this.checkBox7.Size = new Size(80, 17);
      this.checkBox7.TabIndex = 5;
      this.checkBox7.Text = "Show NVM";
      this.checkBox7.UseVisualStyleBackColor = true;
      this.checkBox5.AutoSize = true;
      this.checkBox5.Location = new Point(12, 110);
      this.checkBox5.Name = "checkBox5";
      this.checkBox5.Size = new Size(120, 17);
      this.checkBox5.TabIndex = 4;
      this.checkBox5.Text = "Highlight unknown2";
      this.checkBox5.UseVisualStyleBackColor = true;
      this.checkBox4.AutoSize = true;
      this.checkBox4.Location = new Point(12, 87);
      this.checkBox4.Name = "checkBox4";
      this.checkBox4.Size = new Size(102, 17);
      this.checkBox4.TabIndex = 3;
      this.checkBox4.Text = "Highlight Fading";
      this.checkBox4.UseVisualStyleBackColor = true;
      this.checkBox3.AutoSize = true;
      this.checkBox3.Location = new Point(12, 63);
      this.checkBox3.Name = "checkBox3";
      this.checkBox3.Size = new Size(75, 17);
      this.checkBox3.TabIndex = 2;
      this.checkBox3.Text = "Show Grid";
      this.checkBox3.UseVisualStyleBackColor = true;
      this.checkBox2.AutoSize = true;
      this.checkBox2.Checked = true;
      this.checkBox2.CheckState = CheckState.Checked;
      this.checkBox2.Location = new Point(12, 40);
      this.checkBox2.Name = "checkBox2";
      this.checkBox2.Size = new Size(92, 17);
      this.checkBox2.TabIndex = 1;
      this.checkBox2.Text = "Show Objects";
      this.checkBox2.UseVisualStyleBackColor = true;
      this.checkBox1.AutoSize = true;
      this.checkBox1.Checked = true;
      this.checkBox1.CheckState = CheckState.Checked;
      this.checkBox1.Location = new Point(12, 19);
      this.checkBox1.Name = "checkBox1";
      this.checkBox1.Size = new Size(89, 17);
      this.checkBox1.TabIndex = 0;
      this.checkBox1.Text = "Show Terrain";
      this.checkBox1.UseVisualStyleBackColor = true;
      this.groupBox2.Controls.Add((Control) this.checkBox6);
      this.groupBox2.Controls.Add((Control) this.button3);
      this.groupBox2.Controls.Add((Control) this.label10);
      this.groupBox2.Controls.Add((Control) this.label9);
      this.groupBox2.Controls.Add((Control) this.label8);
      this.groupBox2.Controls.Add((Control) this.textBox2);
      this.groupBox2.Controls.Add((Control) this.textBox1);
      this.groupBox2.Controls.Add((Control) this.label7);
      this.groupBox2.Controls.Add((Control) this.label6);
      this.groupBox2.Controls.Add((Control) this.label5);
      this.groupBox2.Controls.Add((Control) this.label4);
      this.groupBox2.Controls.Add((Control) this.numericUpDown3);
      this.groupBox2.Controls.Add((Control) this.numericUpDown2);
      this.groupBox2.Controls.Add((Control) this.numericUpDown1);
      this.groupBox2.Controls.Add((Control) this.trackBar1);
      this.groupBox2.Enabled = false;
      this.groupBox2.Location = new Point(754, 362);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new Size(148, 256);
      this.groupBox2.TabIndex = 10;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Selected Object";
      this.checkBox6.AutoSize = true;
      this.checkBox6.Location = new Point(9, 83);
      this.checkBox6.Name = "checkBox6";
      this.checkBox6.Size = new Size(95, 17);
      this.checkBox6.TabIndex = 14;
      this.checkBox6.Text = "Distance Fade";
      this.checkBox6.UseVisualStyleBackColor = true;
      this.checkBox6.CheckedChanged += new EventHandler(this.checkBox6_CheckedChanged);
      this.button3.Location = new Point(20, 56);
      this.button3.Name = "button3";
      this.button3.Size = new Size(105, 23);
      this.button3.TabIndex = 13;
      this.button3.Text = "Change Type";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new EventHandler(this.button3_Click);
      this.label10.AutoSize = true;
      this.label10.Location = new Point(46, 41);
      this.label10.Name = "label10";
      this.label10.Size = new Size(33, 13);
      this.label10.TabIndex = 12;
      this.label10.Text = "None";
      this.label9.AutoSize = true;
      this.label9.Location = new Point(6, 41);
      this.label9.Name = "label9";
      this.label9.Size = new Size(34, 13);
      this.label9.TabIndex = 11;
      this.label9.Text = "Type:";
      this.label8.AutoSize = true;
      this.label8.Location = new Point(6, 22);
      this.label8.Name = "label8";
      this.label8.Size = new Size(58, 13);
      this.label8.TabIndex = 10;
      this.label8.Text = "Unique ID:";
      this.textBox2.Location = new Point(62, 19);
      this.textBox2.Name = "textBox2";
      this.textBox2.Size = new Size(80, 20);
      this.textBox2.TabIndex = 9;
      this.textBox1.Location = new Point(62, 186);
      this.textBox1.Name = "textBox1";
      this.textBox1.ReadOnly = true;
      this.textBox1.Size = new Size(80, 20);
      this.textBox1.TabIndex = 8;
      this.label7.AutoSize = true;
      this.label7.Location = new Point(6, 188);
      this.label7.Name = "label7";
      this.label7.Size = new Size(50, 13);
      this.label7.TabIndex = 7;
      this.label7.Text = "Rotation:";
      this.label6.AutoSize = true;
      this.label6.Location = new Point(6, 154);
      this.label6.Name = "label6";
      this.label6.Size = new Size(17, 13);
      this.label6.TabIndex = 6;
      this.label6.Text = "Z:";
      this.label5.AutoSize = true;
      this.label5.Location = new Point(6, 130);
      this.label5.Name = "label5";
      this.label5.Size = new Size(17, 13);
      this.label5.TabIndex = 5;
      this.label5.Text = "Y:";
      this.label4.AutoSize = true;
      this.label4.Location = new Point(6, 106);
      this.label4.Name = "label4";
      this.label4.Size = new Size(17, 13);
      this.label4.TabIndex = 4;
      this.label4.Text = "X:";
      this.numericUpDown3.Location = new Point(37, 104);
      this.numericUpDown3.Maximum = new Decimal(new int[4]
      {
        1000000,
        0,
        0,
        0
      });
      this.numericUpDown3.Minimum = new Decimal(new int[4]
      {
        10000000,
        0,
        0,
        int.MinValue
      });
      this.numericUpDown3.Name = "numericUpDown3";
      this.numericUpDown3.Size = new Size(105, 20);
      this.numericUpDown3.TabIndex = 3;
      this.numericUpDown3.ValueChanged += new EventHandler(this.numericUpDown3_ValueChanged);
      this.numericUpDown2.Location = new Point(37, 128);
      this.numericUpDown2.Maximum = new Decimal(new int[4]
      {
        10000000,
        0,
        0,
        0
      });
      this.numericUpDown2.Minimum = new Decimal(new int[4]
      {
        10000000,
        0,
        0,
        int.MinValue
      });
      this.numericUpDown2.Name = "numericUpDown2";
      this.numericUpDown2.Size = new Size(105, 20);
      this.numericUpDown2.TabIndex = 2;
      this.numericUpDown2.ValueChanged += new EventHandler(this.numericUpDown2_ValueChanged);
      this.numericUpDown1.Location = new Point(37, 152);
      this.numericUpDown1.Maximum = new Decimal(new int[4]
      {
        1000000,
        0,
        0,
        0
      });
      this.numericUpDown1.Minimum = new Decimal(new int[4]
      {
        1000000,
        0,
        0,
        int.MinValue
      });
      this.numericUpDown1.Name = "numericUpDown1";
      this.numericUpDown1.Size = new Size(105, 20);
      this.numericUpDown1.TabIndex = 1;
      this.numericUpDown1.ValueChanged += new EventHandler(this.numericUpDown1_ValueChanged);
      this.trackBar1.Location = new Point(20, 209);
      this.trackBar1.Maximum = 628;
      this.trackBar1.Name = "trackBar1";
      this.trackBar1.Size = new Size(104, 45);
      this.trackBar1.TabIndex = 0;
      this.trackBar1.Scroll += new EventHandler(this.trackBar1_Scroll);
      this.groupBox3.Controls.Add((Control) this.button6);
      this.groupBox3.Controls.Add((Control) this.button5);
      this.groupBox3.Controls.Add((Control) this.button4);
      this.groupBox3.Enabled = false;
      this.groupBox3.Location = new Point(751, 246);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new Size(151, 110);
      this.groupBox3.TabIndex = 11;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Modify";
      this.button6.Location = new Point(7, 77);
      this.button6.Name = "button6";
      this.button6.Size = new Size(138, 23);
      this.button6.TabIndex = 2;
      this.button6.Text = "Delete Current";
      this.button6.UseVisualStyleBackColor = true;
      this.button6.Click += new EventHandler(this.button6_Click);
      this.button5.Location = new Point(7, 48);
      this.button5.Name = "button5";
      this.button5.Size = new Size(138, 23);
      this.button5.TabIndex = 1;
      this.button5.Text = "Clone Current";
      this.button5.UseVisualStyleBackColor = true;
      this.button5.Click += new EventHandler(this.button5_Click);
      this.button4.Location = new Point(7, 19);
      this.button4.Name = "button4";
      this.button4.Size = new Size(138, 23);
      this.button4.TabIndex = 0;
      this.button4.Text = "Add Object";
      this.button4.UseVisualStyleBackColor = true;
      this.button4.Click += new EventHandler(this.button4_Click);
      this.saveFileDialog1.Filter = "Object Map|*.o2";
      this.button7.Location = new Point(754, 45);
      this.button7.Name = "button7";
      this.button7.Size = new Size(148, 22);
      this.button7.TabIndex = 12;
      this.button7.Text = "Open NVM";
      this.button7.UseVisualStyleBackColor = true;
      this.button7.Click += new EventHandler(this.button7_Click);
      this.button8.Location = new Point(754, 66);
      this.button8.Name = "button8";
      this.button8.Size = new Size(148, 22);
      this.button8.TabIndex = 13;
      this.button8.Text = "Save NVM";
      this.button8.UseVisualStyleBackColor = true;
      this.button8.Click += new EventHandler(this.button8_Click);
      this.saveFileDialog2.Filter = "Collision map|*.nvm";
      this.saveFileDialog2.Title = "Save collisionmap";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(1068, 630);
      this.Controls.Add((Control) this.button8);
      this.Controls.Add((Control) this.button7);
      this.Controls.Add((Control) this.groupBox3);
      this.Controls.Add((Control) this.groupBox2);
      this.Controls.Add((Control) this.groupBox1);
      this.Controls.Add((Control) this.listBox3);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.listBox2);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.listBox1);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.glControl1);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = nameof (Form1);
      this.Text = "SRO Map Editor";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.numericUpDown3.EndInit();
      this.numericUpDown2.EndInit();
      this.numericUpDown1.EndInit();
      this.trackBar1.EndInit();
      this.groupBox3.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public Form1()
    {
      this.mapLoaded = false;
      this.ObjNames = new ObjectNames();
      this.MouseWheel += new MouseEventHandler(this.onMouseWheel);
      this.InitializeComponent();
    }

    private void glControl1_Load(object sender, EventArgs e)
    {
      GL.ClearColor(Color.Black);
      int width = this.glControl1.Width;
      int height = this.glControl1.Height;
      GL.MatrixMode(MatrixMode.Projection);
      GL.LoadIdentity();
      GL.Ortho(0.0, 1920.0, 0.0, 1920.0, -5000.0, 5000.0);
      GL.Viewport(0, 0, width, height);
      GL.PointSize(3f);
      GL.Enable(EnableCap.Blend);
      GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
      GL.Enable(EnableCap.DepthTest);
      Application.Idle += new EventHandler(this.Application_Idle);
    }

    private void Application_Idle(object sender, EventArgs e)
    {
      while (this.glControl1.IsIdle)
      {
        if (this.form2 == null)
          this.Render();
        else
          this.form2.RenderGL();
      }
    }

    private void Render()
    {
      if (!this.glControl1.Context.IsCurrent)
        this.glControl1.MakeCurrent();
      GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);
      GL.MatrixMode(MatrixMode.Modelview);
      GL.LoadIdentity();
      List<int> intList = new List<int>();
      if (this.mapLoaded)
      {
        GL.PushMatrix();
        GL.Translate(960f, 960f, 0.0f);
        GL.Scale(this.zoom, this.zoom, this.zoom);
        GL.Rotate((double) this.rotHor / 100.0, 0.0, 1.0, 0.0);
        GL.Rotate((double) this.rotVert / 100.0, 1.0, 0.0, 0.0);
        GL.Translate(-960f, -960f, 0.0f);
        if (this.terrainmap != null && this.checkBox1.Checked)
          this.terrainmap.Draw();
        if (this.checkBox2.Checked)
        {
          for (int index = 0; index < this.Objects.Count; ++index)
          {
            GL.PushMatrix();
            GL.Translate(this.Objects[index].X, this.Objects[index].Z, this.Objects[index].Y);
            GL.Rotate((double) this.Objects[index].Theta * (180.0 / Math.PI), 0.0, 0.0, 1.0);
            if (this.checkBox4.Checked && this.Objects[index].DistFade)
              GL.Color3((byte) 50, byte.MaxValue, (byte) 50);
            if (this.checkBox5.Checked && this.Objects[index].Unknown2 == "0100")
              GL.Color3((byte) 50, (byte) 50, byte.MaxValue);
            if (index == this.selectedObj)
              GL.Color3(byte.MaxValue, (byte) 100, (byte) 100);
            this.Objects[index].Draw();
            if (index == this.selectedObj)
            {
              GL.Color3(byte.MaxValue, byte.MaxValue, byte.MaxValue);
              this.Objects[index].drawBoundingBox();
            }
            GL.PopMatrix();
            if (this.checkBox3.Checked && index == this.selectedObj)
              this.Objects[index].drawGroups();
            GL.Color3(byte.MaxValue, byte.MaxValue, byte.MaxValue);
          }
        }
        if (this.checkBox3.Checked)
        {
          if (this.rotHor != 0)
            this.rotHor = 0;
          if (this.rotVert != 0)
            this.rotVert = 0;
          GL.Color3(byte.MaxValue, (byte) 0, byte.MaxValue);
          int num1 = 6;
          int num2 = 24;
          for (int index1 = 0; index1 < num2; ++index1)
          {
            for (int index2 = 0; index2 < num1; ++index2)
            {
              GL.Begin(BeginMode.LineStrip);
              GL.Vertex3(index1 * (1920 / num2), index2 * (1920 / num1), 500);
              GL.Vertex3((index1 + 1) * (1920 / num2), index2 * (1920 / num1), 500);
              GL.Vertex3((index1 + 1) * (1920 / num2), (index2 + 1) * (1920 / num1), 500);
              GL.Vertex3(index1 * (1920 / num2), (index2 + 1) * (1920 / num1), 500);
              GL.End();
            }
          }
          GL.Color3(byte.MaxValue, byte.MaxValue, byte.MaxValue);
        }
        GL.PopMatrix();
      }
      this.glControl1.SwapBuffers();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      if (this.openFileDialog1.ShowDialog() != DialogResult.OK)
        return;
      string path = this.openFileDialog1.FileName.Substring(0, this.openFileDialog1.FileName.LastIndexOf('.')) + ".m";
      if (File.Exists(path))
      {
        this.terrainmap = new Terrain(path);
      }
      else
      {
        int num = (int) MessageBox.Show("Terrain not found.");
      }
      OFile ofile = new OFile(this.openFileDialog1.FileName);
      this.OX = ofile.OX;
      this.OY = ofile.OY;
      this.Text = "SRO Map Viewer - X: " + (object) this.OX + " Y:" + (object) this.OY;
      this.Objects = ofile.objects;
      List<MeshTexture[]> meshTextureArrayList = new List<MeshTexture[]>();
      foreach (MapObject mapObject in this.Objects)
      {
        if (this.ObjNames.names[mapObject.nameI].Contains(".bsr"))
        {
          mapObject.LoadFiles(this.pathPref, this.ObjNames.names[mapObject.nameI]);
          meshTextureArrayList.Add(mapObject.readMaterial());
        }
      }
      foreach (MeshTexture[] meshTextureArray in meshTextureArrayList)
      {
        foreach (MeshTexture meshTexture in meshTextureArray)
        {
          if (meshTexture != null && !this.texNames.Contains(meshTexture.name))
          {
            this.texNames.Add(meshTexture.name);
            this.texIDs.Add(meshTexture.ID);
          }
        }
      }
      foreach (MapObject mapObject in this.Objects)
        mapObject.FindTex(this.texNames, this.texIDs);
      this.mapLoaded = true;
      this.groupBox3.Enabled = true;
      this.button2.Enabled = true;
      this.fillLists();
    }

    private void onGLMouseDown(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Right || this.dragging)
        return;
      this.dragging = true;
      this.dragStart = e.Location;
    }

    private void onMouseWheel(object sender, MouseEventArgs e)
    {
      if (!this.mapLoaded || this.PointToClient(e.Location).X >= this.glControl1.Width || this.PointToClient(e.Location).Y >= this.glControl1.Height)
        return;
      this.zoom += (double) e.Delta / 300.0;
      if (this.zoom >= 0.1)
        return;
      this.zoom = 0.1;
    }

    private void onGLMouseMove(object sender, MouseEventArgs e)
    {
      if (!this.dragging)
        return;
      this.rotHor += e.Location.X - this.dragStart.X;
      this.rotVert += e.Location.Y - this.dragStart.Y;
    }

    private void onGLMouseUp(object sender, MouseEventArgs e)
    {
      this.dragging = false;
    }

    private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.listBox1.SelectedIndex == -1)
        return;
      this.selectedObj = int.Parse(((string) this.listBox1.SelectedItem).Split(' ')[0]);
      this.listBox2.SelectedIndex = -1;
      this.listBox3.SelectedIndex = -1;
      this.groupBox2.Enabled = true;
      this.textBox2.Text = this.Objects[this.selectedObj].ID.ToString();
      this.label10.Text = ((string) this.listBox1.SelectedItem).Split(' ')[2];
      this.textBox1.Text = this.Objects[this.selectedObj].Theta.ToString();
      this.numericUpDown1.Value = (Decimal) (int) this.Objects[this.selectedObj].Z;
      this.numericUpDown2.Value = (Decimal) (int) this.Objects[this.selectedObj].Y;
      this.numericUpDown3.Value = (Decimal) (int) this.Objects[this.selectedObj].X;
      this.checkBox6.Checked = this.Objects[this.selectedObj].DistFade;
      if ((double) this.Objects[this.selectedObj].Theta < 0.0)
        this.Objects[this.selectedObj].Theta += 6.283185f;
      this.trackBar1.Value = (int) ((double) this.Objects[this.selectedObj].Theta * 100.0);
    }

    private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.listBox2.SelectedIndex == -1)
        return;
      this.selectedObj = int.Parse(((string) this.listBox2.SelectedItem).Split(' ')[0]);
      this.listBox1.SelectedIndex = -1;
      this.listBox3.SelectedIndex = -1;
      this.groupBox2.Enabled = true;
      this.textBox2.Text = this.Objects[this.selectedObj].ID.ToString();
      this.label10.Text = ((string) this.listBox2.SelectedItem).Split(' ')[2];
      this.textBox1.Text = this.Objects[this.selectedObj].Theta.ToString();
      this.numericUpDown1.Value = (Decimal) this.Objects[this.selectedObj].Z;
      this.numericUpDown2.Value = (Decimal) this.Objects[this.selectedObj].Y;
      this.numericUpDown3.Value = (Decimal) this.Objects[this.selectedObj].X;
      this.checkBox6.Checked = this.Objects[this.selectedObj].DistFade;
      if ((double) this.Objects[this.selectedObj].Theta < 0.0)
        this.Objects[this.selectedObj].Theta += 6.283185f;
      this.trackBar1.Value = (int) ((double) this.Objects[this.selectedObj].Theta * 100.0);
    }

    private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (this.listBox3.SelectedIndex == -1)
        return;
      this.selectedObj = int.Parse(((string) this.listBox3.SelectedItem).Split(' ')[0]);
      this.listBox2.SelectedIndex = -1;
      this.listBox1.SelectedIndex = -1;
      this.groupBox2.Enabled = true;
      this.textBox2.Text = this.Objects[this.selectedObj].ID.ToString();
      this.label10.Text = ((string) this.listBox3.SelectedItem).Split(' ')[2];
      this.textBox1.Text = this.Objects[this.selectedObj].Theta.ToString();
      this.numericUpDown1.Value = (Decimal) this.Objects[this.selectedObj].Z;
      this.numericUpDown2.Value = (Decimal) this.Objects[this.selectedObj].Y;
      this.numericUpDown3.Value = (Decimal) this.Objects[this.selectedObj].X;
      this.checkBox6.Checked = this.Objects[this.selectedObj].DistFade;
      if ((double) this.Objects[this.selectedObj].Theta < 0.0)
        this.Objects[this.selectedObj].Theta += 6.283185f;
      this.trackBar1.Value = (int) ((double) this.Objects[this.selectedObj].Theta * 100.0);
    }

    private void trackBar1_Scroll(object sender, EventArgs e)
    {
      this.Objects[this.selectedObj].setRotation((float) this.trackBar1.Value / 100f);
      this.textBox1.Text = ((float) this.trackBar1.Value / 100f).ToString();
    }

    private void numericUpDown3_ValueChanged(object sender, EventArgs e)
    {
      this.Objects[this.selectedObj].X = (float) this.numericUpDown3.Value;
    }

    private void numericUpDown2_ValueChanged(object sender, EventArgs e)
    {
      this.Objects[this.selectedObj].Y = (float) this.numericUpDown2.Value;
    }

    private void numericUpDown1_ValueChanged(object sender, EventArgs e)
    {
      this.Objects[this.selectedObj].Z = (float) this.numericUpDown1.Value;
    }

    private void button3_Click(object sender, EventArgs e)
    {
      this.form2 = new Form2(this.ObjNames, this.Objects[this.selectedObj].nameI);
      int num = (int) this.form2.ShowDialog();
      int selectedItem = this.form2.SelectedItem;
      this.form2 = (Form2) null;
      if (!this.glControl1.Context.IsCurrent)
        this.glControl1.MakeCurrent();
      if (selectedItem == -1)
        return;
      this.Objects[this.selectedObj].nameI = selectedItem;
      this.Objects[this.selectedObj].LoadFiles(this.pathPref, this.ObjNames.names[selectedItem]);
      foreach (MeshTexture[] meshTextureArray in new List<MeshTexture[]>()
      {
        this.Objects[this.selectedObj].readMaterial()
      })
      {
        foreach (MeshTexture meshTexture in meshTextureArray)
        {
          if (meshTexture != null && !this.texNames.Contains(meshTexture.name))
          {
            this.texNames.Add(meshTexture.name);
            this.texIDs.Add(meshTexture.ID);
          }
        }
      }
      this.Objects[this.selectedObj].FindTex(this.texNames, this.texIDs);
      this.fillLists();
    }

    private void button4_Click(object sender, EventArgs e)
    {
      this.form2 = new Form2(this.ObjNames, -1);
      int num = (int) this.form2.ShowDialog();
      int selectedItem = this.form2.SelectedItem;
      this.form2 = (Form2) null;
      if (!this.glControl1.Context.IsCurrent)
        this.glControl1.MakeCurrent();
      if (selectedItem == -1)
        return;
      MapObject mapObject = new MapObject();
      mapObject.nameI = selectedItem;
      mapObject.LoadFiles(this.pathPref, this.ObjNames.names[selectedItem]);
      foreach (MeshTexture[] meshTextureArray in new List<MeshTexture[]>()
      {
        mapObject.readMaterial()
      })
      {
        foreach (MeshTexture meshTexture in meshTextureArray)
        {
          if (meshTexture != null && !this.texNames.Contains(meshTexture.name))
          {
            this.texNames.Add(meshTexture.name);
            this.texIDs.Add(meshTexture.ID);
          }
        }
      }
      mapObject.FindTex(this.texNames, this.texIDs);
      mapObject.X = 0.0f;
      mapObject.Y = 0.0f;
      mapObject.Z = 0.0f;
      mapObject.Theta = 0.0f;
      mapObject.ID = this.genUniqueID();
      mapObject.DistFade = true;
      this.Objects.Add(mapObject);
      this.selectedObj = this.Objects.Count - 1;
      this.fillLists();
    }

    private void button5_Click(object sender, EventArgs e)
    {
      MapObject mapObject = new MapObject();
      mapObject.nameI = this.Objects[this.selectedObj].nameI;
      mapObject.LoadFiles(this.pathPref, this.ObjNames.names[mapObject.nameI]);
      foreach (MeshTexture[] meshTextureArray in new List<MeshTexture[]>()
      {
        mapObject.readMaterial()
      })
      {
        foreach (MeshTexture meshTexture in meshTextureArray)
        {
          if (meshTexture != null && !this.texNames.Contains(meshTexture.name))
          {
            this.texNames.Add(meshTexture.name);
            this.texIDs.Add(meshTexture.ID);
          }
        }
      }
      mapObject.FindTex(this.texNames, this.texIDs);
      mapObject.X = this.Objects[this.selectedObj].X;
      mapObject.Y = this.Objects[this.selectedObj].Y;
      mapObject.Z = this.Objects[this.selectedObj].Z;
      mapObject.Theta = this.Objects[this.selectedObj].Theta;
      mapObject.ID = this.genUniqueID();
      this.Objects.Add(mapObject);
      this.selectedObj = this.Objects.Count - 1;
      this.fillLists();
    }

    private void button6_Click(object sender, EventArgs e)
    {
      if (this.selectedObj != -1)
      {
        this.Objects.RemoveAt(this.selectedObj);
        this.selectedObj = -1;
        this.fillLists();
      }
      else
      {
        int num = (int) MessageBox.Show("Select an object first!");
      }
    }

    private void fillLists()
    {
      this.listBox1.Items.Clear();
      this.listBox2.Items.Clear();
      this.listBox3.Items.Clear();
      for (int index = 0; index < this.Objects.Count; ++index)
      {
        string name = this.ObjNames.names[this.Objects[index].nameI];
        if (name.Contains("\\bldg\\"))
        {
          this.listBox1.Items.Add((object) (index.ToString() + " - " + name.Substring(name.LastIndexOf('\\') + 1)));
          if (index == this.selectedObj)
            this.listBox1.SelectedIndex = this.listBox1.Items.Count - 1;
        }
        else if (name.Contains("\\nature\\"))
        {
          this.listBox2.Items.Add((object) (index.ToString() + " - " + name.Substring(name.LastIndexOf('\\') + 1)));
          if (index == this.selectedObj)
            this.listBox2.SelectedIndex = this.listBox2.Items.Count - 1;
        }
        else
        {
          this.listBox3.Items.Add((object) (index.ToString() + " - " + name.Substring(name.LastIndexOf('\\') + 1)));
          if (index == this.selectedObj)
            this.listBox3.SelectedIndex = this.listBox3.Items.Count - 1;
        }
      }
    }

    private int genUniqueID()
    {
      int num = 0;
      List<int> intList = new List<int>();
      foreach (MapObject mapObject in this.Objects)
        intList.Add(mapObject.ID);
      while (intList.Contains(num))
        ++num;
      return num;
    }

    private void button2_Click(object sender, EventArgs e)
    {
      if (this.saveFileDialog1.ShowDialog() != DialogResult.OK)
        return;
      this.saveMap(this.saveFileDialog1.FileName);
    }

    private void saveMap(string path)
    {
      List<int>[] intListArray = new List<int>[144];
      for (int index1 = 0; index1 < this.Objects.Count; ++index1)
      {
        if (this.Objects[index1].groups == null)
          this.Objects[index1].calcGroups();
        else if (this.Objects[index1].groups.Count == 0)
          this.Objects[index1].calcGroups();
        for (int index2 = 0; index2 < this.Objects[index1].groups.Count; ++index2)
        {
          if (intListArray[this.Objects[index1].groups[index2]] == null)
            intListArray[this.Objects[index1].groups[index2]] = new List<int>();
          intListArray[this.Objects[index1].groups[index2]].Add(index1);
        }
      }
      BinaryWriter binaryWriter = new BinaryWriter((Stream) File.Open(path, FileMode.Create));
      binaryWriter.Write("JMXVMAPO1001".ToCharArray());
      for (int index1 = 0; index1 < 144; ++index1)
      {
        if (intListArray[index1] == null)
        {
          binaryWriter.Write((short) 0);
        }
        else
        {
          binaryWriter.Write((short) intListArray[index1].Count);
          for (int index2 = 0; index2 < intListArray[index1].Count; ++index2)
          {
            MapObject mapObject = this.Objects[intListArray[index1][index2]];
            binaryWriter.Write(mapObject.nameI);
            float x = mapObject.X;
            if ((double) mapObject.X < 0.0)
              x += 1920f;
            if ((double) mapObject.X > 1920.0)
              x -= 1920f;
            binaryWriter.Write(x);
            binaryWriter.Write(mapObject.Y);
            float z = mapObject.Z;
            if ((double) mapObject.Z < 0.0)
              z += 1920f;
            if ((double) mapObject.Z > 1920.0)
              z -= 1920f;
            binaryWriter.Write(z);
            if (!mapObject.DistFade)
              binaryWriter.Write(new byte[2]
              {
                byte.MaxValue,
                byte.MaxValue
              });
            else
              binaryWriter.Write((short) 0);
            binaryWriter.Write(mapObject.Theta);
            binaryWriter.Write(mapObject.ID);
            if (mapObject.Unknown1 == "0100")
              binaryWriter.Write((short) 1);
            else
              binaryWriter.Write((short) 1);
            binaryWriter.Write((byte) ((double) this.OX + Math.Floor((double) mapObject.X / 1920.0)));
            binaryWriter.Write((byte) ((double) this.OY + Math.Floor((double) mapObject.Z / 1920.0)));
          }
        }
      }
      binaryWriter.Close();
      int num = (int) MessageBox.Show("File saved");
    }

    private void checkBox6_CheckedChanged(object sender, EventArgs e)
    {
      this.Objects[this.selectedObj].DistFade = this.checkBox6.Checked;
    }

    private void button7_Click(object sender, EventArgs e)
    {
      if (this.folderBrowserDialog1.ShowDialog() != DialogResult.OK)
        return;
      this.nvmFile = new NVM(this.folderBrowserDialog1.SelectedPath + "\\nv_" + this.OY.ToString("X").ToLower() + this.OX.ToString("X").ToLower() + ".nvm");
    }

    private void button8_Click(object sender, EventArgs e)
    {
      this.saveFileDialog2.FileName = this.nvmFile.filepath;
      this.nvmFile.setEntities(this.Objects, this.pathPref, this.ObjNames.names);
      if (this.saveFileDialog2.ShowDialog() != DialogResult.OK)
        return;
      this.nvmFile.saveNVM(this.saveFileDialog2.FileName, this.OX, this.OY);
    }
  }
}
