﻿// Decompiled with Syinea's decompiler
// Type: SroMapEditor.DDSReader.PixelFormat
// Assembly: SroMapEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 02A4606F-D062-4F97-A318-DADC65FCD6AD
// Assembly location: C:\Users\Syinea\Downloads\Mapeditor\Mapeditor\SroMapEditor.exe

namespace SroMapEditor.DDSReader
{
  internal enum PixelFormat
  {
    ARGB,
    RGB,
    DXT1,
    DXT2,
    DXT3,
    DXT4,
    DTX5,
    THREEDC,
    ATI1N,
    LUMINANCE,
    LUMINANCE_ALPHA,
    RXGB,
    A16B16G16R16,
    R16F,
    G16R16F,
    A16B16G16R16F,
    R32F,
    G32R32F,
    A32B32G32R32F,
    UNKNOWN,
  }
}
