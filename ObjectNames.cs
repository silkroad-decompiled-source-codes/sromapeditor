﻿// Decompiled with Syinea's decompiler
// Type: SroMapEditor.ObjectNames
// Assembly: SroMapEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 02A4606F-D062-4F97-A318-DADC65FCD6AD
// Assembly location: C:\Users\Syinea\Downloads\Mapeditor\Mapeditor\SroMapEditor.exe

using System;
using System.IO;
using System.Windows.Forms;

namespace SroMapEditor
{
  public class ObjectNames
  {
    public string[] names;

    public ObjectNames()
    {
      if (!File.Exists("Map\\object.ifo"))
      {
        int num = (int) MessageBox.Show("Could not find object.ifo. Terminating application.");
        Environment.Exit(1);
      }
      string[] strArray = File.ReadAllLines("Map\\object.ifo");
      this.names = new string[strArray.Length - 2];
      for (int index = 0; index < strArray.Length - 2; ++index)
      {
        string str1 = strArray[index + 2].Substring(strArray[index + 2].IndexOf(' ') + 1);
        string str2 = str1.Substring(str1.IndexOf(' ') + 1);
        string str3 = str2.Remove(str2.Length - 1).Substring(1);
        this.names[index] = str3;
      }
    }
  }
}
